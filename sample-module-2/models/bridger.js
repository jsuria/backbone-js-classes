/**
 * BridgerModel - Extends the global Common model for local use
 *
 * JavaScript
 *
 * @category  Special Handling / Contract Management
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */ 

define([
    'underscore',
    MBSJS.path('models/common'),
    MBSJS.path('application/deferred')
], function(
    _,
    CommonModel,
    MbsDeferred
) {

    var BridgerModel = CommonModel.extend({

        /**
         * Init methods and properties done on parent class/model
         */

        /**
         * Gets contract tiers
         * 
         * @return object
         */
        getContractTiers: function()
        {
            return this.wsCommon.call({
                method: 'GetBixgContractTiers',
                data: {},
                async: true
            });
        },

        /**
         * Distills tiers and returns them as an array
         * 
         * @return MbsDeferred
         */
        getTierDropdown: function()
        {
            var tierDeferred = new MbsDeferred();

            $.when(
                this.getContractTiers()
            ).done(function(result){

                var types = result.getData();
                tierDeferred.resolve(types);
            }); 

            return tierDeferred;
        },
        
        /**
         * Handles Bridger calculation
         *
         * @param {object} params         
         *
         * @return object                          
         */ 
        getBridgerCalculation: function(params)
        {
            return this.wsCommon.call({
                method: 'GetBridgerCalculation',
                data: {
                    full_bridger_insight_xg_contract_amount: params.calc_contract_amt,
                    percentage_discount_applied: params.calc_discount,
                    tier_customer_support_provided: params.calc_tier 
                },

                // if any post-calculation work needs done
                success: function(){},
                error: function(){},

                async: true
            });
        },

        /**
         * Gets distilled results
         *
         * @param {object} params
         *
         * @return MbsDeferred
         * 
         */ 
        getTotalFees: function(params)
        {   
            var feesDeferred = new MbsDeferred();

            $.when(this.getBridgerCalculation(params)).done(function(feesList){
                var feesTotal = feesList.getData();

                feesDeferred.resolve(feesTotal);
            });

            return feesDeferred;
        }
    });

    return BridgerModel;
});