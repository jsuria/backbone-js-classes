
/**
 * Index / Bridger Insights Calculator launcher
 *
 * JavaScript
 *
 * @category  Special Handling / Contract Management
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
define([
    'underscore',
    'AppManager',
    'handlebars',
    'MbsView',
    'translate',
   
    MBSJS.App.path('modules/gblsh/views/calculator/main'),
    'text!modules/gblsh/templates/calculator/index.html',
    
    // Template helpers
    "hbsContainerBlock",
    "hbsTranslate"
	], function( 
        _,
        AppManager,
        Handlebars,
        MbsView,
        translate,
        CalculatorView,
	    template
	){
		var IndexView = MbsView.extend({

            el: $('#app-container'),
            template: null,
            permissions: {
                render : "GBLSH_VIEWCONTRACTS"
            },
            callbacks: [],
            events: {},

            /**
             * Initialize
             *
             */
            init: function()
            {                
                this.template = Handlebars.compile(template);
            },

            /**
             * Render myself then calculator
             *
             * @return void
             */
            render: function()
            {
                var self = this;

                // Listener for render completion
                this.onOnce('onRenderComplete', function(e){
                    // Remove any existing views
                    if (!_.isUndefined(self.CalculatorView)) {
                        self.CalculatorView.destroy();
                    }
                
                    self.CalculatorView = new CalculatorView();

                    // Render the calculator
                    self.CalculatorView
                        .setElement(self.$el.find('div#container-calculator'))
                        .render({});
                });

                this.$el.html(this.template({}));
            }
        });

    return IndexView;
});