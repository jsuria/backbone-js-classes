/**
 * Bridger Insights Calculator
 *
 * JavaScript
 *
 * @category  Special Handling/Contract Management
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */

define([
    'underscore',
    'handlebars',
    'MbsView',
    'MbsLoader',
    'translate',
    MBSJS.App.path('modules/gblsh/models/bridger'),
    'text!modules/gblsh/templates/calculator/main.html',

    // Additional Template Helpers
    "hbsDataTable",
    "hbsFormInputControl",
    "hbsTranslate"
], function( 
    _,
    Handlebars,
    MbsView,
    MbsLoader,
    translate,
    BridgerModel,
    template
) {
    var CalculatorView = MbsView.extend({

        el:       $('#app-container'),
        template: null,    
        permissions: {
                render : "GBLSH_VIEWCONTRACTS"
        },
        callbacks: [],
        events: {
            "input input#calc_contract_amt"  : "doInputValidation",
            "input input#calc_discount"      : "doInputValidation",
            "change input#calc_contract_amt" : "doBlurAmountValidation",
            "change input#calc_discount"     : "doBlurDiscountValidation",
            "change select#calc_tier"        : "doTierValidation",
            "submit form#frm-bixg-calculator": "doCalculation",

            "click #btn-calculate"           : "doSubmit",
            "click #btn-reset"               : "doReset",

            "click #btn-copy-fee"            : "doTotalFeeCopy",
            "click #btn-copy-license"        : "doTotalLicenseCopy"            
        },

        selectors: {

            TOOLTIP_ALL      : '[data-toggle="tooltip"]',

            TOOLTIP_AMOUNT   : 'i#tip_contract_amount',
            TOOLTIP_DISCOUNT : 'i#tip_discount',
            TOOLTIP_TIER     : 'i#tip_tier',
            TOOLTIP_CMD_SHOW : 'show',
            TOOLTIP_CMD_HIDE : 'hide',

            TOTAL_FEE        : 'input#calc_total_fee',
            TOTAL_LICENSE    : 'input#calc_total_license',

            FORM_CALCULATOR  : 'form#frm-bixg-calculator',
            FORM_TOTALS      : 'form#frm-bixg-totals'
        },

        messages: {
            MSG_AMOUNT_EMPTY  : "Please provide a contract amount.",
            MSG_AMOUNT_FORMAT : "The amount format is invalid.",
            MSG_AMOUNT_RANGE  : "Amount provided is out of range.",
            MSG_DISCOUNT_RANGE : "Percentage must be between 0-100"
        },

        /**
         * Initialize
         *
         */
        init: function()
        {
            this.template = Handlebars.compile(template);
            this.BridgerModel = new BridgerModel();

            this.validated = true;
        },

        /**
         * Submit for Bridger calculation
         *
         * @param {event} e
         *
         * @return void         
         */
        doCalculation: function (e)
        {
            var self = this;

            if (e) {
                e.preventDefault();
            }

            // validation
            var formData = this.serialize();

            if(!this.isSet(formData.calc_contract_amt)){
                $(this.selectors.TOOLTIP_AMOUNT).tooltip(this.selectors.TOOLTIP_CMD_SHOW);
                this.validated = false;
            }

            if(!this.isSet(formData.calc_discount)){
                $(this.selectors.TOOLTIP_DISCOUNT).tooltip(this.selectors.TOOLTIP_CMD_SHOW);   
                this.validated = false;
            }

            if(!this.isSet(formData.calc_tier)){
                $(this.selectors.TOOLTIP_TIER).tooltip(this.selectors.TOOLTIP_CMD_SHOW);
                this.validated = false;                
            }

            if(this.validated){

                MbsLoader.start();

                $.when(
                    self.BridgerModel.getTotalFees(formData)
                ).done(function(results){

                    MbsLoader.end();

                    var totals = results.getData(0);

                    // Populate total fields
                    // Round off to 2 places
                    $(self.selectors.TOTAL_FEE).val(
                        self.parseToRounded(
                            totals.bridger_insight_xg_premier_support_fee
                        )
                    );

                    $(self.selectors.TOTAL_LICENSE).val(
                        self.parseToRounded(
                            totals.bridger_insight_xg_license
                        )
                    );
                });    
            }
        },

        /**
         * Realtime input validation
         *
         * @param {event} e
         *
         * @return void         
         */
        doInputValidation: function(e)
        {
            if(e){
                e.preventDefault();
            }

            // Hide tooltips
            $(this.selectors.TOOLTIP_ALL).tooltip(this.selectors.TOOLTIP_CMD_HIDE);

            // Just lets user enter valid input
            var _val = $(e.target).val(),            
                _rxp = /[^0-9\d\.\x08]/g;

            // Enforce valid input
            var _new_val = _val.replace(_rxp, '');

            // Trim leading zeroes
            var _nonzero_val = _new_val.replace(/^0+/g,'');
            
            $(e.target).val(_nonzero_val);                
        },

        /**
         * Additional validation on blur
         *
         * @param {event} e
         *
         * @return void         
         */
        doBlurAmountValidation: function(e)
        {
            if(e){
                e.preventDefault();
            }

            var _val = $(e.target).val();

            // Parse as proper float and round of 2 places
            // Enforces input to be xxxxx.xx
            $(e.target).val(this.parseToRounded(_val));
        },

        /**
         * Additional validation on blur
         *
         * @param {event} e
         *
         * @return void         
         */
        doBlurDiscountValidation: function(e)
        {
            if(e){
                e.preventDefault();
            }

            var _val = $(e.target).val();

            // Parse as proper float and round of 2 places
            // Enforces input to be xxxxx.xx
            var _rounded = this.parseToRounded(_val);

             // Ensure this value is within range
             // Flag as invalid if out of range
            if(parseFloat(_rounded) < 0 || parseFloat(_rounded) > 100){
                $(this.selectors.TOOLTIP_DISCOUNT).tooltip(this.selectors.TOOLTIP_CMD_SHOW);

                // highlight the value
                $(e.target).select();
                this.validated = false;
            } else {
                $(e.target).val(_rounded);
                this.validated = true;
            }               
        },

        /**
         * Checks if tier option is selected
         *
         * @param {event} e
         *
         * @return void         
         */
        doTierValidation: function(e)
        {
            if(e){
                e.preventDefault();
            }   

            // Hide tooltips
            $(this.selectors.TOOLTIP_ALL).tooltip(this.selectors.TOOLTIP_CMD_HIDE);

            // For now, serves to reset the validation flag
            // Add more logic here when needed
            this.validated = true;
        },


        /**
         * Copies selected total text
         *
         * @param {event} e
         *
         * @return void         
         */
        doTotalFeeCopy: function(e)
        {
            if(e){
                e.preventDefault();
            }

            // Select the text
            $(this.selectors.TOTAL_FEE).select();
            this.copySelected();
        },

        /**
         * Copies selected total text
         *
         * @param {event} e
         *
         * @return void         
         */
        doTotalLicenseCopy: function(e)
        {
            if(e){
                e.preventDefault();
            }

            // Select the text
            $(this.selectors.TOTAL_LICENSE).select();
            this.copySelected();
        },        

        /**
         * Wrapper event for form submit
         *
         * @param {event} e
         *
         * @return void         
         */
        doSubmit: function(e)
        {   
            if (e) {
                e.preventDefault();
            }

            $(this.selectors.FORM_CALCULATOR).trigger('submit');
        },

        /**
         * Wrapper event for form reset
         *
         * @param {event} e
         *
         * @return void         
         */
        doReset: function(e)
        {
            if (e) {
                e.preventDefault();
            }

            $(
                this.selectors.FORM_CALCULATOR + 
                ',' + 
                this.selectors.FORM_TOTALS
            ).trigger('reset');
        },

        /**
         * Utility to check for empty or zero value
         *
         * @param {string} field
         *
         * @return boolean        
         */
        isSet: function(field)
        {
            return !_.isEmpty(field) && parseInt(field) !== 0;
        },

        /**
         * Utility for converting and rounding off value
         *
         * @param {float} val
         *
         * @return float         
         */
        parseToRounded: function(val)
        {
            return (
                parseFloat(val)
            ).toFixed(2);
        },

        /**
         * Utility for copying selected text to system clipboard
         *
         * @return void         
         */
        copySelected: function()
        {
            document.execCommand(
                'copy', 
                false, 
                null
            );
        },

        /**
         * Main Render Method
         *
         */
        render: function ()
        {
            var self = this;

            $.when(
                self.BridgerModel.getTierDropdown()
            ).done(function(results){

                var list = results.getData(0);

                _.each(list.bixg_contract_tier ,function(obj, key) {
                    
                    // Populate tier dropdown when done
                    $('<option>')
                        .text(obj.bixg_tier_desc)
                        .val(obj.tier_num)
                        .appendTo('select#calc_tier');
                });

                // forcibly enable the select dropdown (oh my!)
                // handlebars currently disables this on load time if there are no values
                $('select#calc_tier').prop('disabled',false);
            });  

            // Renzerrr zee template-uh!
            // Zee tierzzz optionz vill load laterr...
            self.$el.html(
                self.template({})
            );

            // hide help blocks
            $('.help-block').hide();

            // Initialize tooltips
            // No bells and whistles
            $(this.selectors.TOOLTIP_ALL).tooltip({
                            animation: false,
                            container: false,
                            placement: "right"
                        });
        }
    });

    return CalculatorView;
});