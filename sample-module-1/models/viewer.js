/**
 * ViewerModel - Extends the global Lockbox model for local use
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */ 

define([
    'underscore',
    MBSJS.path('models/lockbox'),
], function(
    _,
    LockboxModel
) {

    var ViewerModel = LockboxModel.extend({

        /**
         * Init methods and properties done on parent class/model
         */

        /**
         * Returns file-mapping types for lockboxes
         * 
         * @return object
         */
        getMappingTypes: function()
        {
            // Async is disabled in order to pre-process the list
            // TODO: In the future, enable this and find a way to 
            // populate dropdown after template has loaded
            return this.wsLockbox.call({
                method: 'GetLockboxFileMappings',
                data: {},
                async: false
            });
        },

        /**
         * Pre-processes mapping types and extracts the default selection
         * 
         * @return array
         */
        getMapTypeDropdown: function()
        {
            var list = [], 
                defaultId = null;

            var self = this;

            $.when(
                self.getMappingTypes()
            ).done(function(result){

                var types = result.getData();

                _.each(types ,function(obj, key) {

                    list.push({
                        Key : obj.MappingName,              // for display
                        Value : obj.LockboxFileMappingId    // value of the option
                    });

                    if(obj.IsDefault){
                        defaultId = obj.LockboxFileMappingId;
                    }
                });

            }); 

            return {
                data: list,
                default: defaultId
            };
        },
        
        /**
         * Handles Lockbox Payment posting
         *
         * @param {object}   filePath
         * @param {int}      mappingId
         * @param {boolean}  suppressWSError
         *
         * @return object                          
         */ 
        sendFileForProcessing: function(filePath, mappingId, suppressWSError)
        {
            // Suppress WS error dialog and handle on view
            if(suppressWSError){
               this.wsLockbox.suppressAllErrorDialogs(suppressWSError);
            }

            var self = this;

            return this.wsLockbox.call({
                method: 'ParseLockboxFile',
                data: {
                    FileInBase64: filePath,
                    LockboxFileMappingId: mappingId
                },

                // Always reset dialog suppression every successful send
                // Only this WS call will need to hide dialog
                success: function(){
                    self.wsLockbox.suppressAllErrorDialogs(false);
                },

                error: function(){                
                    self.wsLockbox.suppressAllErrorDialogs(false);
                },

                async: true
            });
        },

        /**
         * Get the processed lockbox file
         *
         * @param {object} filePath
         * @param {int}    mappingId
         *
         * @return json
         * 
         */ 
        getProcessedFileInfo: function(filePath, mappingId)
        {   
            var list = [];

            $.when(this.sendFileForProcessing(filePath, mappingId)).done(function(infoList){
                list = infoList.getData();
            });

            return list;
        }
    });

    return ViewerModel;
});