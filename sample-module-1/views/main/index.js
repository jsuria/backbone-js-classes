
/**
 * LockBoxView
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
define([
    'underscore',
    'handlebars',
    'AppManager',
    'MbsView',
    'MbsLoader',
    'translate',
    MBSJS.App.path('modules/lckbx/models/viewer'),
    MBSJS.path('components/dialogs/modal'),
    MBSJS.App.path('modules/lckbx/libs/utils'),
    MBSJS.App.path('modules/lckbx/libs/ui'),
    MBSJS.App.path('modules/lckbx/libs/constants'),
    MBSJS.App.path('modules/lckbx/views/main/viewer'),
    MBSJS.App.path('modules/lckbx/views/main/attach'),
    MBSJS.path('components/data/tabs'),
    MBSJS.path('application/deferred'),
    MBSJS.path('components/dialogs/alert'),
    'text!modules/lckbx/templates/main/index.html',
    
    // Template helpers
    "hbsFormInputControl",
    "hbsContainerBlock",
    "hbsTranslate"
	], function( 
        _,
        Handlebars,
        AppManager,
        MbsView,
        MbsLoader,
        translate,
        ViewerModel,
        Modal,
        Utils,
        UI,
        Constants,
        FileViewer,
        AttachView,
        Tabs,
        MbsDeferred,
        Alert,
	    template
	){
		var IndexView = MbsView.extend({

            el:      $('#app-container'),
            template: null,
            permissions:
            {
                "render": "LCKBX_FILE_VIEWER"
            },
            callbacks: [],
            events: {
                "click #btn-attach-file"   : "doAttachFile",
                "click #btn-process-file"  : "doProcessFile",
                "change #lockbox_mapping"  : "doHideMapping"
            },

            /**
             * view initialization
             *
             * @return void
             */
            init: function()
            {
                this.template = Handlebars.compile(template);

                this.viewer = new ViewerModel();
                this.tabView = new Tabs(Constants.view.INDEX_TABVIEW_ID);
            },

            /**
             * Hides the mapping type message
             *
             * @param {event} e
             *
             * @return void
             */
            doHideMapping: function(e)
            {
                if (e) {
                    e.preventDefault();
                }

                UI.hide([
                    Constants.view.INDEX_CSSDN_FILE_MAPPING
                    ], 
                    false
                );
            },

            /**
             * Attach file using dropzone inside modal
             *
             * @param {event} e
             *
             * @return void
             */
            doAttachFile: function(e)
            {
                var self = this;

                if (e) {
                    e.preventDefault();
                }

                if (!_.isUndefined(this.attachView)) {
                    this.attachView.destroy();
                }
            
                this.attachView = new AttachView();

                this.attachView.onOnce('onImportComplete', function() {
                    UI.hide([
                        Constants.view.INDEX_CSSDN_FILE_PATH
                        ], 
                        false
                    );

                    self.modal.hide();
                });

                // Create new modal
                this.modal = new Modal(
                    self.attachView,
                    {
                        title: translate(Constants.view.INDEX_MODAL_TITLE),
                        width:  Constants.view.INDEX_MODAL_WIDTH
                    }
                );

                this.modal.show();
            },

            /**
             * Process file, send to WS and distill into a searchable format
             *
             * @param {event} e
             *
             * @return void
             */
            doProcessFile: function(e)
            {
                if (e) {
                    e.preventDefault();
                }

                var self = this;

                // Remove existing instance
                if (!_.isUndefined(self.fileViewer)) {
                    self.fileViewer.destroy();
                }

                // Remove existing cache
                if (!_.isUndefined(Utils.getCacheObject(Constants.keys.CACHE_GLOBAL_ID))) {                    
                    Utils.clearCacheObject(Constants.keys.CACHE_GLOBAL_ID)
                }                

                self.fileViewer = new FileViewer({
                    viewLoader: true
                });

                // Serialize form data
                var formData = this.serialize(),
                    formResults = null;

                // Ensure file and map types are specified
                if( _.isEmpty(formData.lockbox_filepath) ){
                    UI.show([
                        Constants.view.INDEX_CSSDN_FILE_PATH
                    ], false);

                    return false;
                }

                if(_.isEmpty(formData.lockbox_mapping) ){
                    UI.show([
                        Constants.view.INDEX_CSSDN_FILE_MAPPING
                    ], false);

                    return false;
                }

                MbsLoader.start();

                $.when(
                    this.viewer.sendFileForProcessing(
                        formData.lockbox_data64, 
                        formData.lockbox_mapping,
                        true
                ))
                .done(
                    function(wsResult){

                        MbsLoader.end();

                        // Success
                        if(_.isObject(wsResult) && wsResult.successful){

                            formResults = wsResult.getData();
                        
                            // Empty variable to reduce overhead
                            wsResult = {};

                            // Return cache object
                            Utils.cacheJSON(Constants.keys.CACHE_GLOBAL_ID, formResults);

                            // Now parse further, check for UNKNOWN line types
                            var jsonObject = Utils.getCacheObject(Constants.keys.CACHE_GLOBAL_ID);

                            if(jsonObject.isValidJSONTransmission()){

                                // Render viewer
                                self.fileViewer
                                    .setElement(self.$el.find(Constants.view.INDEX_RESULT_AREA))
                                    .render({
                                        formData : formData                        
                                    });
                            } else {

                                // File is not recognized format
                                if (!_.isUndefined(self.fileViewer)) {
                                    self.fileViewer.destroy();
                                }

                                // Clear the active viewer area
                                UI.resetHTML([
                                    Constants.view.INDEX_RESULT_AREA
                                ]);

                                UI.setResultArea(
                                    Constants.view.INDEX_RESULT_PLACEHOLDER,
                                    Constants.view.INDEX_RESULT_AREA
                                );

                                Alert.notice(Constants.view.ALERT_MESSAGES['nolinetype']);
                            }

                        // Failure, usually WS error
                        // WS error is suppressed on model side
                        } else {

                            if (!_.isUndefined(self.fileViewer)) {
                                self.fileViewer.destroy();
                            }

                            // Clear the active viewer area
                            UI.resetHTML([
                                Constants.view.INDEX_RESULT_AREA
                            ]);

                            UI.setResultArea(
                                Constants.view.INDEX_RESULT_PLACEHOLDER,
                                Constants.view.INDEX_RESULT_AREA
                                );

                            Alert.notice(
                                wsResult.message + ' ' +
                                Constants.view.ALERT_MESSAGES['fileformat']
                            );
                        }
                        // Attempt to clear memory
                        // Will remove this after further testing
                        formResults = null;
                        formData = null;
                    }
                );
            },

            /**
             * Render method
             *
             * @return void
             */
            render: function()
            {
                var self = this,
                    result = this.viewer.getMapTypeDropdown(),
                    list = result.data;

                self.$el.html(
                    self.template({
                        mapping: list
                    })
                );

                // Set the default value
                $(Constants.view.INDEX_FORM_FILE_MAPPING).val(result.default);

                // Hide all help text;
                UI.hide([
                            Constants.view.INDEX_HELP_BLOCK
                        ], 
                        false
                );
            }
        });

    return IndexView;
});