/**
 * LockBox File Attach
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */

define([
    'underscore',
    'handlebars',
    'AppManager',
    'MbsView',
    MBSJS.App.path('modules/lckbx/libs/constants'),
    MBSJS.App.path('modules/lckbx/libs/utils'),
    'text!modules/lckbx/templates/main/attach.html',

    // Additional Template Helpers
    "hbsDropZoneControl",
    "hbsTranslate"
], function( 
    _,
    Handlebars,
    AppManager,
    MbsView,
    Constants,
    Utils,
    template
) {
    var AttachView = MbsView.extend({

        el:       $('#app-container'),
        template: null,    
        permissions:
        {
            "render": "LCKBX_FILE_VIEWER"
        },
        callbacks: 
        [
            "onImportComplete"
        ],
        events:
        {
            "filesReady [name=drop-zone]": "doProcessDroppedFile"
        },

        /**
         * Main View Constructor
         *
         */
        init: function()
        {
            this.template = Handlebars.compile(template);
        },

        /**
         * Actions when a file is dropped         
         * @param {event} e
         * @param {file} droppedFiles
         *
         * @return void         
         */
        doProcessDroppedFile: function (e, droppedFiles)
        {
            var self = this;

            if (e) {
                e.preventDefault();
            }

            // TODO: Check for massive file size. 
            // Approx 1MB potentially cause issues in local testing
            $.when(Utils.convertFileToBase64(droppedFiles[0].data)).done(function(result){

                // Base64
                $(Constants.view.ATTACH_DATA64).val(result);
                // Raw string
                $(Constants.view.ATTACH_DATARAW).val(droppedFiles[0].data);
                // Name of file
                $(Constants.view.ATTACH_FILEPATH).val(droppedFiles[0].name);

                self.notifyCallbackListeners('onImportComplete');
            });
        },

        /**
         * Main Render Method
         *
         */
        render: function ()
        {
            var self = this;

            // Render the template
            self.$el.html(
                self.template({})
            );
        }

    });

    return AttachView;
});
