
/**
 * LockBoxView
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
define([
    'underscore',
    'backbone',
    'handlebars',
    'AppManager',
    'MbsView',
    'translate',
    MBSJS.path('models/lockbox'),
    MBSJS.path('application/deferred'),
    MBSJS.path('components/dialogs/alert'),
    MBSJS.App.path('modules/lckbx/libs/constants'),
    MBSJS.App.path('modules/lckbx/libs/utils'),
    MBSJS.App.path('modules/lckbx/libs/search'),
    MBSJS.App.path('modules/lckbx/libs/ui'),
    'text!modules/lckbx/templates/main/viewer.html',

    // Helpers
    "hbsDataTable",
    "hbsFormInputControl",
    "hbsContainerBlock",
    "hbsTranslate"
    ], function( 
        _,
        Backbone,
        Handlebars,
        AppManager,
        MbsView,
        translate,
        LockboxModel,
        MbsDeferred,
        Alert,
        Constants,
        Utils,
        SearchEnabler,
        UI,
	    template
	){
		var FileViewer = MbsView.extend({

            el:       $("#app-container"),
            template: null,
            permissions:
            {
                "render": "LCKBX_FILE_VIEWER"
            },
            callbacks: [],
            params: null,
            events: {
                "click #btn-find-search"                                  : "doSearch",
                "click #btn-find-next"                                    : "doNext",
                "click #btn-find-prev"                                    : "doPrev",
                "input input#search_keywords"                             : "doInputValidate",
                "keydown input#search_keywords"                           : "doKeyValidate",
                "paste input#search_keywords"                             : "doPasteValidate",
                "click #table-raw-string table tbody tr[id^='row_'] td"   : "doGetStorageItem",
                "change select#search_filter_linetypes"                   : "doGetFields",
                "change select#search_filter_fields"                      : "doResetOnChangeField"
            },

            columns: [
                {
                    field: "Line",
                    name: translate("Line")
                },
                {
                    field: "Value",
                    name: translate("Raw string")
                }
            ],

            /**
             * view initialization
             *
             */
            init: function()
            {
                this.template = Handlebars.compile(template);
                this.searchEngine = null;
                this.cacheGlobal = null;
                this.numMatches = 0;
            },

            /**
             * Performs JSON search
             *
             * @param {event} e
             * 
             * @return void
             */
            doSearch: function(e)
            {
                var self = this;
                
                if (e) {
                    e.preventDefault();
                }

                var _validated = true;

                if( _.isObject(self.searchEngine) ) {
                    self.searchEngine = null;
                }

                // Ensure keywords and filters are not empty
                // Otherwise, display messages
                if(_.isEmpty( $(Constants.view.VIEWER_SEARCH_KEYWORDS).val() )){
                    UI.show([Constants.view.VIEWER_FILTER_BLOCKSEARCH], false);
                    _validated = false;
                }

                if(_.isEmpty( $(Constants.view.VIEWER_SEARCH_FILTERFIELD).val() )){
                    UI.show([Constants.view.VIEWER_FILTER_BLOCKFIELD], false);
                    _validated = false;
                }

                if(_.isEmpty( $(Constants.view.VIEWER_SEARCH_FILTERLINE).val() )){
                    UI.show([Constants.view.VIEWER_FILTER_BLOCKLINE], false);
                    _validated = false;
                }

                if(_validated){

                    // Create instance of search after rendering page
                    self.searchEngine = new SearchEnabler({
                        needle     : Constants.view.VIEWER_SEARCH_KEYWORDS,
                        hayStack   : Constants.view.VIEWER_SEARCH_TABLE,
                        filters    : 
                        {
                            fields: Constants.view.VIEWER_SEARCH_FILTERFIELD,
                            linetypes: Constants.view.VIEWER_SEARCH_FILTERLINE
                        }
                    });

                    var _num_matches = self.searchEngine.run();
                
                    if(_num_matches > 0){

                        // Save copy for use in iteration display
                        self.numMatches = _num_matches;

                        // Only enable when there are multiple results
                        if(_num_matches > 1){
                            $(Constants.view.VIEWER_SEARCH_BTPREV).prop('disabled', false);
                            $(Constants.view.VIEWER_SEARCH_BTNEXT).prop('disabled', false);    
                        }                        
                        // Get reference to first result, and iterate there
                        var _first = self.searchEngine.getFirstPointer();
                        self.iterate(_first);

                    } else {
                        Alert.notice(Constants.view.ALERT_MESSAGES['nomatch']);
                    }
                }
            },

            /**
             * Gets stored details on each raw string row click
             * 
             * @param event e
             * 
             * @return boolean
             */
            doGetStorageItem: function(e)
            {
                if (e) {
                    e.preventDefault();
                }

                var self = this;

                if(!UI.getRowDetails(e.target)){
                    
                    self.resetUI(true);
                    Alert.notice(Constants.view.ALERT_MESSAGES['nullrow']);
                }
            },

            /**
             * Get fields after choosing a line type
             *
             * @param {event} e
             * 
             * @return void
             */
            doGetFields: function(e)
            {
                var _type = $(e.target).val();

                UI.hide([
                            Constants.view.VIEWER_FILTER_BLOCKSEARCH,
                            Constants.view.VIEWER_FILTER_BLOCKFIELD,
                            Constants.view.VIEWER_FILTER_BLOCKLINE        
                        ],
                        false
                    );
            
                if(!_.isEmpty(_type)){
                    UI.setFilterFields(_type);    
                } else {
              
                    UI.resetSelect([
                        {
                            selector   : Constants.view.VIEWER_SEARCH_FILTERFIELD,
                            defaultVal : Constants.view.VIEWER_FIELD_DEFAULTVALUE,
                            disable    : true
                        }
                    ]);
                }
            },

            /**
             * Resets search UI after change in field select
             *
             * @param {event} e
             * 
             * @return void
             */
            doResetOnChangeField: function(e)
            {
                if (e) {
                    e.preventDefault();
                }

                this.resetUI(false);
            },

            /**
             * Handler for next button
             *
             * @param {event} e
             * 
             * @return void
             */
            doNext: function (e) 
            {  
                if (e) {
                    e.preventDefault();
                }

                var _curr = this.searchEngine.getCurrentPointer(false);
                this.iterate(_curr);
            },

            /**
             * Handler for prev button
             *
             * @param {event} e
             * 
             * @return void
             */
            doPrev: function (e) 
            {  
                if (e) {
                    e.preventDefault();
                }

                var _curr = this.searchEngine.getCurrentPointer(true);
                this.iterate(_curr);
            },        

            /**
             * Handle copy-paste into search input
             *
             * @param {event} e
             * 
             * @return void
             */
            doPasteValidate: function(e)
            {
                // Passes delegation to input event
                $(e.target).trigger('input');
            },

            /**
             * Resets the viewer UI on every backspace
             *
             * @param {event} e
             * 
             * @return void
             */
            doKeyValidate: function(e)
            {
                if(
                    e.keyCode === Constants.inputCodes.INPUT_BACKSPACE || 
                    e.key === Constants.inputCodes.INPUT_BACKSPACE_KEY
                ){
                    this.resetUI(true);
                }
            },
            
            /**
             * All-encompassing validation for input
             *
             * @param {event} e
             * 
             * @return void
             */
            doInputValidate: function(e)
            {
                if (e) {
                    e.preventDefault();
                }

                var self = this,
                    _value = $(e.target).val();
                
                // Screen out invalid input
                $(e.target).val(Utils.screenKeyInput(_value));

                // Spaces and backspace only
                if(_.isEmpty(_value)){        
                    self.resetUI(true);
                }
            },

            /**
             * Iterate through all matches
             *
             * @param {object} currentObject
             * 
             * @return void
             */
            iterate: function(currentObject)
            {               
                var _focus = UI.scrollToFocus(Constants.view.VIEWER_PREFIX_ROW_ID + currentObject.id);                
                
                UI.showMatchMessage(
                    "Showing match (" +  currentObject.onebased_index + ") of " + this.numMatches
                );

                // Execute click on current row to get details
                // Event is captured on td, so need to do a find
                $(_focus).find('td').trigger("click");

                // Need to get reference to matching row in details table
                // and trigger the click event on that row
                var _selected_field = $(Constants.view.VIEWER_SEARCH_FILTERFIELD).val(),
                    _target_row = 'tr[mapped-to="span-' + _selected_field + '"] td';

                $(Constants.view.VIEWER_DETAILS_TABLE).find(_target_row)
                    .first()
                    .trigger('click');
            },

            /**
             * Consolidated call to clear values
             *
             * @param {boolean} includeFilters
             * 
             * @return void
             */
            resetUI: function(includeFilters)
            {
                var self = this;

                // Scroll to top row
                var _top = $(Constants.view.VIEWER_SEARCH_TABLETOP).first();
                UI.scrollToFocus(_top.attr('id'));

                // Disable prev button                    
                $(Constants.view.VIEWER_SEARCH_BTPREV).prop('disabled', true);

                // Disable next button                    
                $(Constants.view.VIEWER_SEARCH_BTNEXT).prop('disabled', true);

                if(includeFilters){

                    UI.resetSelect([
                        {
                            selector  : Constants.view.VIEWER_SEARCH_FILTERLINE,
                            defaultVal: Constants.view.VIEWER_LINETYPE_DEFAULTVALUE,
                            disable   : false
                        },
                        {
                            selector  : Constants.view.VIEWER_SEARCH_FILTERFIELD,
                            defaultVal: Constants.view.VIEWER_FIELD_DEFAULTVALUE,
                            disable   : true
                        }
                    ]);

                }

                // Removes highlights from table
                // and resets the detail tables    
                UI.resetHighlight([
                    {
                        selector : Constants.view.VIEWER_SEARCH_TABLE + ' ' + '.' + Constants.view.VIEWER_ROW_SELECTCLASS,
                        remove   : Constants.view.VIEWER_ROW_SELECTCLASS
                    }
                ]);

                UI.resetHTML([
                                Constants.view.VIEWER_DETAILS_TABLE,
                                Constants.view.VIEWER_DETAILS_RAWSTRING,
                                Constants.view.VIEWER_LINETYPE_TABLE
                            ]);

                // Hides the block text
                UI.hide([
                            Constants.view.VIEWER_FILTER_BLOCKSEARCH,
                            Constants.view.VIEWER_FILTER_BLOCKFIELD,
                            Constants.view.VIEWER_FILTER_BLOCKLINE        
                        ],
                        false
                        );

                // Resets search handler instance
                if( _.isObject(self.searchEngine) ) {                    
                    self.searchEngine = null;
                }
            },

            /**
             * Render method
             *
             * @return void
             */
            render: function(params)
            {
                var self = this;

                $.when(
                    Utils.convertStringtoData(params.formData.lockbox_raw_data)
                ).done(function(arrayData){

                    self.cacheGlobal = Utils.getCacheObject(Constants.keys.CACHE_GLOBAL_ID);

                    self.$el.html(
                        self.template({
                            data   : arrayData,
                            colMap : self.columns,
                            searchFiltersLineTypes : self.cacheGlobal.getUIAllLineTypes(true)
                        })
                    );

                    // Initiate selector binding
                    UI.config();

                    // Clear
                    params.lockbox_raw_data = null;

                    // Hide all help text;
                    UI.hide([".help-block"], 
                            false
                        );
                });
            }
        });

    return FileViewer;
});