/**
 * Module.js 
 *
 * JavaScript
 *
 * @category  Category
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */

define([
    'MbsConfig'
], function(
    MbsConfig
) {
    // Return Config Object
    return new MbsConfig({
        'routes':
        {
            /**
             * Renders an HTML template located at the given folder / template pair (path)
             *
             * @return void
             */
            "*actions": function()
            {
                this.dispatch("modules/lckbx/views/main/index");
            }
        }   
    });
});