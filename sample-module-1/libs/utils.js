/**
 * Lockbox Viewer Utilities
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */

define([
	'underscore',
	MBSJS.App.path('modules/lckbx/libs/constants'),
	MBSJS.App.path('modules/lckbx/libs/jsonmap')
	], function(
	_,
	Constants,
	JsonMap
	) {
	/** 
     * Constructor 
     */
    var Utils = function Utils(){
    	// Set default options here, if needed    	
    	this.constants = Constants.keys;
        this.constantsRegex = Constants.inputRegex;
    };

    Utils.prototype = {

     	/**
         * Convert to Base64
         *
         * @param  {string} str - file string for processing
         * 
         * @return {string} - converted binary string         
         */
        convertFileToBase64: function(str)
        {
            // For now, just convert the string
            return btoa(str);
        },

        /**
         * Saves the cache and attaches it to document
         *
         * @param  {string} cacheId - unique identifier
         * @param {object} obj - object to be cached
         * 
         * @return void
         */
        saveCacheObject: function(cacheId, object)
        {
        	$(Constants.keys.CACHE_ELEMENT).data(cacheId, object);
        },

        /**
         * Returns the cache object
         *
         * @param  {string} cacheId - unique identifier
         * 
         * @return object
         */
        getCacheObject: function(cacheId)
        {
        	return $(Constants.keys.CACHE_ELEMENT).data(cacheId);
        },

        /**
         * Unsets the cache object
         *
         * @param  {string} cacheId - unique identifier
         * 
         * @return object
         */
        clearCacheObject: function(cacheId)
        {        
            $(Constants.keys.CACHE_ELEMENT).removeData(cacheId);
        },        

        /**
         * Store json-format WS result and map it to make it more navigable
         *
         * @param {string} cacheId - unique identifier
         * @param {object} object - json object to be cached
         * 
         * @return void
         */
        cacheJSON: function(cacheId, object)
        {
            var self = this;

            var _jsonmap_object = new JsonMap(object);

            if(_.isObject(_jsonmap_object)){
            	_jsonmap_object.init();
            	self.saveCacheObject(cacheId, _jsonmap_object);
            }
        },

     	/**
	     * Convert raw string to object array
	     *
	     * @param {string} str
         * 
	     * @return array
	     */
        convertStringtoData: function(str)
        {
            var tmp = str.split("\n");

            str = null;

            for(var i = 0, len = tmp.length ; i < len; i++){
                tmp[i] = {
                	Value : (tmp[i]).trim(),
                	Line : i
                };
            }

            return tmp;
        },

       /**
         * Find all invalid input and blank them out
         *
         * @param  {string} stringInput
         * 
         * @return string
         */
        screenKeyInput: function(stringInput)
        {      
            return stringInput.replace(
                Constants.inputRegex.REGEX_NONALPHANUMERIC,
                Constants.inputRegex.REGEX_REPLACEMENT
            );
        }
     };

     var UtilsSingleton = UtilsSingleton || new Utils();
     return UtilsSingleton;
});