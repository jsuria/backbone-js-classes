/**
 * Map - A navigable wrapper for large JSON objects
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
define([
	'underscore',
	MBSJS.App.path('modules/lckbx/libs/constants'), 
	MBSJS.App.path('modules/lckbx/libs/storage')
	], function(
	_,
	Constants,
	Storage
) {
	/** 
     * Constructor 
     */
	var JsonMap = function JsonMap(jsonObject){		   

		this.object = jsonObject;

		// base object for caching search data
		this.fieldsbylinenumber = new Storage();

		// base object for caching UI rows
		this.fieldsuibylinenumber = new Storage();

		// all fields after flatten operation
		this.fields = [];

		// Line types with corresponding field names
		this.linetypes = new Storage();

		this.constants = Constants.keys;
	};

	JsonMap.prototype = {

		/**
		 * Pre-sets the field types, names and objects
		 * 
		 * @return void
		 */
		init: function()
		{
			// Preserve reference to this instance
			var self = this;

			// get all fields, with the line number as key
			_.each(_.flatten(self.object), function(lineVal,lineKey){

				// Numeric line key with object array value
				var fieldObject = {},
					fieldUIObject = {};

				fieldObject[self.constants.FLD_LINE_TYPE] = lineVal[self.constants.FLD_LINE_TYPE];
				fieldObject[self.constants.FLD_LINE_NUMBER] = lineVal[self.constants.FLD_LINE_NUMBER];
				fieldObject[self.constants.FLD_LINE_DESCRIPTION] = lineVal[self.constants.FLD_LINE_DESCRIPTION];

				// Simply copy over properties (At this point, fieldObject is still a plain object)
				// We can't clone everything since both objects have different purposes
				fieldUIObject = _.clone(fieldObject);

				// copy over the parsed fields with the index info
				if(lineVal[self.constants.FLD_PARSED_FIELDS]){
					fieldUIObject[self.constants.FLD_PARSED_FIELDS] = lineVal[self.constants.FLD_PARSED_FIELDS];

					// iterate over field columns, add to main object as plain key
					_.each(lineVal[self.constants.FLD_PARSED_FIELDS], function(fieldVal, fieldKey){ 
						fieldObject[fieldVal[self.constants.FLD_FIELD_NAME]] = fieldVal[self.constants.FLD_FIELD_VALUE];
					});

					// Sort the parsed fields by start index
					var _sortAlgo = function(ths, tht)
					{					
						var cmp = self.constants.FLD_FIELD_START_INDEX;
						
						if (ths[cmp] < tht[cmp]){
							return -1;
						}

						if (ths[cmp] > tht[cmp]){
							return 1;
						}

						return 0;
					};

					(lineVal[self.constants.FLD_PARSED_FIELDS]).sort(_sortAlgo);
				}

				// for ui
				self.fieldsuibylinenumber.setItem(lineKey, fieldUIObject);
				self.fieldsbylinenumber.setItem(lineKey, fieldObject);
			});

			self.fields = _.pluck(
								self.fieldsbylinenumber.asArray(), 
								self.constants.FLD_FIELD_VALUE
							);

			var _lineTypes = function()
			{
				return _.uniq(
						_.pluck(
							self.fields,
							self.constants.FLD_LINE_TYPE
							)
						);
			};

			_.each(_lineTypes(), function(lineType, key){
				// Get a sample record of this type and get the keys
				var prop = {};
				
				prop[self.constants.FLD_LINE_TYPE] = lineType;

				self.linetypes.setItem(
									lineType, 
									_.keys
									((
										_.where(
											self.fields,
											prop
											)
										)[0]
									));					
								});	
		},

		/**
		 * Wittles down the returned JSON for valid entries 
		 * by removing 'UNKNOWN' line types. An empty array 
		 * means the returned JSON does not contain data.
		 *
		 * @return boolean
		 */
		isValidJSONTransmission: function()
		{
			var self = this;

			return (_.without(
						_.pluck(
							_.pluck(
								self.fieldsbylinenumber.asArray(),
								self.constants.FLD_FIELD_VALUE
							),
							self.constants.FLD_LINE_TYPE
						),
					self.constants.FLD_INVALID)).length > 0;
		},

		/**
		 * Use this to return ALL lines, with no ParsedFields
		 *
		 * @return array
		 */
		getUIAllLines: function()
		{
			return this.fields;
		},

		/**
		 * Simply returns all field names
		 * If true, returns as an array with key-value pair elements
		 *
		 * @param {boolean} asObjectPairs
		 *
		 * @return void
		 */
		getUIAllFieldnames: function(asObjectPairs)
		{
			var self = this,
				fn = _.uniq(
						_.flatten(
							_.pluck(
								self.linetypes.asArray(),
								self.constants.FLD_FIELD_VALUE
								)
							)
						);			    

			return (asObjectPairs) ? self.convertToKeyPair(fn) : fn;
		},

		/**
		 * Use this in line type filter drop down
		 * If true, returns as an array with key-value pair elements
		 *
		 * @param {boolean} asObjectPairs
		 *
		 * @return array
		 */
		getUIAllLineTypes: function(asObjectPairs)
		{
			var self = this,
				lt   =  _.without(
							_.pluck(
								self.linetypes.asArray(), 
								self.constants.FLD_FIELD_KEY
								), 
								self.constants.FLD_INVALID
							);

			return (asObjectPairs) ? self.convertToKeyPair(lt) : lt;
		},		

		/**
		 * Use this in field filter drop down
		 *
		 * @param {string} lineType
		 *
		 * @return array
		 */
		getUIFieldnamesByLineType: function(lineType)
		{
			var self = this;

			// Strip off the 'LineType', 'LineNumber' and 'LineDescription' fields
			// No need to use as field filters (LineType is already a filter all its own)
			// LineNumber already displayed on the left column.
			// LineDescription search not supported
			return _.without(
						self.linetypes.getItem(lineType), 
						self.constants.FLD_LINE_TYPE,
						self.constants.FLD_LINE_NUMBER,
						self.constants.FLD_LINE_DESCRIPTION
					);
		},
		
		/**
		 * Get "flattened" line item, no ParsedFields array
		 *
		 * @param {int} lineNumber
		 *
		 * @return object
		 */
		getItemByLineNumber: function(lineNumber)
		{
			return this.fieldsbylinenumber.getItem(lineNumber);
		},

		/**
		 * Get line item with ParsedFields array intact
		 *
		 * @param {int} lineNumber
		 *
		 * @return object
		 */
		getUIItemByLineNumber: function(lineNumber)
		{
			return this.fieldsuibylinenumber.getItem(lineNumber);
		},

		/**
		 * Get line Id of specified item
		 *
		 * @param {string} lineType
		 * @param {string} filterKey
		 * @param {string} filterValue
		 *
		 * @return int
		 */
		getLineItem: function(lineType, filterKey, filterValue)
		{
			var self = this, 
				query = {};

				query[self.constants.FLD_LINE_TYPE] = lineType;
				query[filterKey] = filterValue;

			return _.pluck(
				_.flatten(
					_.where(
							self.fields,
							query
						)
					),
				self.constants.FLD_LINE_NUMBER
			);
		},

		/**
		 * Callback that converts plain array elements to object key-value pairs
		 * Key and Value have the same value
		 *
		 * @param {array} array
		 * 
		 * @return array
		 */
		convertToKeyPair: function(array)
		{	   
			return _.map(array, function(val){ 
					return { 
								Key : val, 
								Value : val 
							} 	
					});
		}
	}; 

	return JsonMap;
});