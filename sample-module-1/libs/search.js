/**
 * SearchEnabler
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */

define([
    'underscore',
    MBSJS.App.path('modules/lckbx/libs/constants'),
    MBSJS.App.path('modules/lckbx/libs/utils'),
    MBSJS.App.path('modules/lckbx/libs/list')
], function(
    _,
    Constants,
    Utils,
    List
) {
    /** 
     * Constructor 
     * 
     * @param Array params
     *
     */
    var SearchEnabler = function SearchEnabler(params){
        // Might consider putting default values here in the future   
        this.needle = params.needle;     
        this.filtersFields = params.filters.fields;
        this.filtersLineTypes = params.filters.linetypes;
        this.hayStack = params.hayStack;

        // Globally available json object
        this.hayStackGlobal = Utils.getCacheObject(Constants.keys.CACHE_GLOBAL_ID);

        // Attributes with default values
        this.frmSearch  = params.frmSearch || Constants.view.VIEWER_SEARCH_FORM;
        this.matchClass = params.matchClass || Constants.view.VIEWER_ROW_SELECTCLASS;       
        this.searchList = null;

        this.currentPointer = 0;
    };

    SearchEnabler.prototype = {

        /**
         * Executes the search
         *
         * @return no. of matches
         */
        run : function()
        {
            var self = this;

            self.clearSearch();

            // Perform search
            var _match = self.searchMap();

            if(!_.isEmpty(_match)){

                // Navigable list that can access its next and prev
                self.searchList = new List(_match);

                $.each(_match, function(i, elem){
                    $("#row_" + elem).addClass(self.matchClass);
                });
            }

            return _match.length;
        },

        /**
         * Search algo using json traversal
         *
         * @return array of matches
         */
        searchMap : function()
        {
            var self = this,
                _needle = $(self.needle).val(),
                _type = $(self.filtersLineTypes).val(),
                _field = $(self.filtersFields).val();

            // getLineItem returns an array of match id's
            return self.hayStackGlobal.getLineItem(_type, _field, _needle);
        },

        /**
         * Iterates and focuses to current match
         *
         * @return object containing id and positive index (one-based)
         */
        getCurrentPointer : function(inReverse)
        {
            // Check if pointer is currently set
            if(this.currentPointer === null || this.currentPointer === undefined){
                this.currentPointer = 0;
            }

            // Update the pointer
            if(inReverse){
                this.currentPointer = this.searchList.getPrev(this.currentPointer).key;
            } else {
                this.currentPointer = this.searchList.getNext(this.currentPointer).key;
            }

            var self = this;

            return {
                    id : self.searchList.getNode(this.currentPointer).value,
                    onebased_index : parseInt(self.currentPointer) + 1
                };

        },

        /**
         * Gets first search result
         *
         * @return object containing id and positive index (one-based)
         */
        getFirstPointer: function()
        {
            var _first_node = this.searchList.getStartNode(),
                _first_match = _first_node.value,
                _first_index = _first_node.key;

            return {
                    id : _first_match,
                    onebased_index : parseInt(_first_index) + 1
                };    
        },


        /**
         * Clears for new search
         *
         * @return void
         */
        clearSearch : function()
        {
            this.searchList = null;
            this.currentPointer = null;
        }
    };
 
    return SearchEnabler;
});