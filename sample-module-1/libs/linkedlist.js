/**
 * Lockbox List
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
define([
		'underscore',
		MBSJS.App.path('modules/lckbx/libs/storage')
	], function(
		_,
		Storage
	) {
	 /** 
     * Constructor 
     */
	var LinkedList = function LinkedList(listValues){
		this.list = new Storage();
		this.values = listValues;

		this.alpha = 0;
    	this.omega = this.values.length - 1;

    	var self = this;
    
		var _start_node = this.values[this.alpha];

		// Initializes the start and end nodes
		this.list.setItem(
			self.alpha, 			// Internal key for private use
			{
				key: self.alpha,	// Public key for iterator use
				prev: self.omega,
				value: _start_node,
				next: null
			});

		var _end_node = this.values[self.omega];
		this.list.setItem(
			self.omega, 
			{
				key: self.omega,
				prev: null,
				value: _end_node,
				next: self.alpha
			});

		self.init();
	};

    LinkedList.prototype = {

    	/**
         * Initialize the searchable list
         *         
         * @return void
         */
    	init : function()
    	{
    		// Initialize the start and end nodes
    		var self = this;

    		for(var i = 0, len = self.values.length; i < len; i++){

    			// Get existing node if any
    			var _node = self.list.hasKey(i) ? self.list.getItem(i) : {};
  
    			// Set next node for first node
		    	var _new_node = {
		    		key  : i,
					prev : (i === self.alpha) ? _node.prev : i - 1,
					value: self.values[i],
					next : (i === self.omega) ? _node.next : i + 1
		    	};
    			
    			self.list.setItem(i, _new_node);
    		}
    	},

    	/**
         * Picks a node from the list by key
         * 
         * @param {int} nodeKey    
		 * 
         * @return object
         */
	    getNode: function(nodeKey)
	    {
	    	return this.list.getItem(nodeKey);	
	    },

	    /**
         * Gets the previous node
         * 
         * @param {int} nodeKey    
		 * 
         * @return object
         */
		getPrev: function(nodeKey)
	    {
	    	return this.getNode(
	    		this.getNode(nodeKey).prev
	    	);
	    },

	    /**
         * Gets the next node
         * 
         * @param {int} nodeKey    
		 * 
         * @return object
         */
	    getNext: function(nodeKey)
	    {
	    	return this.getNode(
	    		this.getNode(nodeKey).next
	    	);
	    },

	    /**
         * Gets the first node in the list
         * 
         * @return object
         */
	    getStartNode: function()
	    {
	    	return this.getNode(this.alpha);
	    },

		/**
         * Gets the last node in the list
         * 
         * @return object
         */
	    getEndNode: function()
	    {
	    	return this.getNode(this.omega);
	    }
	};

    return LinkedList;
});