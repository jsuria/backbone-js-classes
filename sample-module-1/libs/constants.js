/**
 * Constants
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */
define([], function() {

	var Constants = function Constants(){
        /**
         * Object properties, variable names
         */

	    this.keys = {

            CACHE_ELEMENT        : "body",
            CACHE_GLOBAL_ID      : "_jsonmap_cache",
         	FLD_PARSED_FIELDS    : 'ParsedFields',
            FLD_FIELD_NAME       : 'FieldName',
            FLD_FIELD_START_INDEX: 'StartIndex',
            FLD_FIELD_LENGTH	 : 'Length',
            FLD_FIELD_VALUE	     : 'Value',
            FLD_FIELD_KEY	     : 'Key',
            FLD_LINE_TYPE	     : 'LineType',
            FLD_LINE_NUMBER	     : 'LineNumber',
            FLD_LINE_DESCRIPTION : 'LineDescription',
            FLD_DESCRIPTION      : 'FieldDescription',
            FLD_INVALID		     : 'UNKNOWN',
            FLD_CHECK_ITEM	     : 'check_item',
            FLD_BATCH_HEADER     : 'batch_header',
            FLD_BATCH_TRAILER    : 'batch_trailer',
            FLD_LOCKBOX_TRAILER  : 'lockbox_trailer',
            FLD_TRANSMISSION_TRAILER: 'transmission_trailer'

        };

        /**
         * DOM objects, selectors
         */
        this.view = {

            INDEX_FORM_FILE_PATH      : "input#lockbox_filepath",
            INDEX_FORM_FILE_MAPPING   : "select#lockbox_mapping",
            INDEX_CSSDN_FILE_PATH     : "input#lockbox_filepath + div.help-block",
            INDEX_CSSDN_FILE_MAPPING  : "select#lockbox_mapping + div.help-block",
            INDEX_RESULT_AREA         : "#result-area",
            INDEX_HELP_BLOCK          : "div.help-block",
            INDEX_RESULT_DIVCLASS     : "alert alert-success",
            INDEX_RESULT_ICLASS       : "fa fa-info-circle",
            INDEX_RESULT_PLACEHOLDER  : " Please upload your file to begin",
            INDEX_MODAL_TITLE         : "Attach Lockbox File",
            INDEX_MODAL_WIDTH         : 600,
            INDEX_TABVIEW_ID          : "result-tabs",

            ATTACH_DATA64             : "div#app-container input#lockbox_data64",
            ATTACH_DATARAW            : "div#app-container input#lockbox_raw_data",
            ATTACH_FILEPATH           : "div#app-container #lockbox_filepath",

            VIEWER_SEARCH_KEYWORDS     : 'input#search_keywords',
            VIEWER_SEARCH_FILTERFIELD  : '#search_filter_fields',
            VIEWER_SEARCH_FILTERLINE   : '#search_filter_linetypes',

            VIEWER_FIELD_DEFAULTVALUE  : 'Please select a field...',
            VIEWER_LINETYPE_DEFAULTVALUE : null,
            VIEWER_PREFIX_ROW_ID       : 'row_',

            VIEWER_FILTER_BLOCKSEARCH  : 'input#search_keywords + div.help-block',
            VIEWER_FILTER_BLOCKFIELD   : '#search_filter_fields + div.help-block',
            VIEWER_FILTER_BLOCKLINE    : '#search_filter_linetypes + div.help-block',

            VIEWER_SEARCH_TABLE        : 'div#table-raw-string table tbody',
            VIEWER_SEARCH_TABLETOP     : 'div#table-raw-string table tbody tr',
            VIEWER_SEARCH_FORM         : 'form#search_form',
            VIEWER_SEARCH_BTPREV       : 'form#search_form button#btn-find-prev',
            VIEWER_SEARCH_BTNEXT       : 'form#search_form button#btn-find-next',
            VIEWER_SEARCH_BTDEFAULT    : 'form#search_form button#btn-find-search',
            
            VIEWER_LINETYPE_TABLE      : 'table#tbl-line-type tbody',
            VIEWER_DETAILS_RAWSTRING   : 'div#text-raw-string',
            VIEWER_DETAILS_SPANCLASS   : 'section-selected',
            VIEWER_DETAILS_TABLE       : 'table#tbl-parsed-details tbody',

            VIEWER_ROW_SELECTCLASS     : 'raw-row-string-selected',

            ALERT_MESSAGES : { 
                        'nomatch' : 'No matches found.',
                        'nullrow' : 'A mapping for this line was not found.',
                        'nolinetype' : "Your file does not contain any known line types.",
                        'fileformat' : "Please check the format of your file and try again."
            }
        };

        /**
         * Reserved for keyboard input codes
         */
        this.inputCodes = {

            INPUT_BACKSPACE : 8,
            INPUT_BACKSPACE_KEY : 'BACKSPACE',
            INPUT_SPACE     : 32,
            INPUT_ZER0      : 48,
            INPUT_ZED       : 90

        };

        /**
         * Reserved for regular expressions
         */
        this.inputRegex = {

            REGEX_NONALPHANUMERIC : /[^a-zA-Z\d\s\x08]/g,
            REGEX_REPLACEMENT : ''

        };
	};

	var ConstantsSingleton = ConstantsSingleton || new Constants();
    return ConstantsSingleton;
});