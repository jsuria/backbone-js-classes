/**
 * Lockbox Viewer UI
 *
 * JavaScript
 *
 * @category  Lockbox
 * @package   MBS
 * @author    Jose Suria <Jose.Suria@lexisnexisrisk.com>
 * @copyright 2017 LexisNexis
 * @license   https://mbsdev.seisint.com/wiki/index.php/License MBS
 * @version   SVN: $Id$
 * @link      https://mbsdev.seisint.com/wiki/
 */

define([
	'underscore',
	MBSJS.App.path('modules/lckbx/libs/utils'),
	MBSJS.App.path('modules/lckbx/libs/constants')
	], function(
		_,
		Utils,
		Constants
	) {
    /** 
     * Constructor 
     */
    var UI = function UI(){
        this.viewerTable      = Constants.view.VIEWER_SEARCH_TABLE;
        this.detailsTable     = Constants.view.VIEWER_DETAILS_TABLE;
        this.lineTypeTable    = Constants.view.VIEWER_LINETYPE_TABLE;
        this.detailsRawString = Constants.view.VIEWER_DETAILS_RAWSTRING;
        this.detailsSpanClass = Constants.view.VIEWER_DETAILS_SPANCLASS;
        this.rowSelectClass   = Constants.view.VIEWER_ROW_SELECTCLASS;
        this.searchForm       = Constants.view.VIEWER_SEARCH_FORM;

        this.constants = Constants.keys;
    };

    UI.prototype = {

        /**
         * Starts building daa attributes into target table
         *        
         * @return void
         */
    	config: function()
    	{
            this.cacheGlobal = Utils.getCacheObject(this.constants.CACHE_GLOBAL_ID);
	        this.setRowAttributes();
    	},

    	/**
         * Sets data to each row in the view table
         *        
         * @return void
         */
    	setRowAttributes: function()
    	{
    		var self = this,
   				_rows = $(self.viewerTable).find('tr');
   			var _parsed = self.cacheGlobal.getUIAllLines();

    		for(var i = 0, len = _rows.length; i < len; i++){

    			var _tr   = _rows[i],
    				_parsed_row = _parsed[i];

    			// store data at this row
    			$(_tr).data(
    				self.constants.FLD_LINE_TYPE, 
    				_parsed_row[self.constants.FLD_LINE_TYPE]
    			);

    			$(_tr).data(
    				self.constants.FLD_LINE_NUMBER, 
    				_parsed_row[self.constants.FLD_LINE_NUMBER]
    			);
    		}
    	},

    	/**
         * Gets stored details on each raw string row click
         *
         * @param {object} elem
         * 
         * @return boolean
         */
    	getRowDetails: function(elem)
    	{
    		var self = this;

    		// Get the siblings instead of parent
            var _cell   = $(elem),
                _row    = _cell.parent('tr'),
				_rowLineNumber = $(_row).data(self.constants.FLD_LINE_NUMBER),
				_rowLineType = $(_row).data(self.constants.FLD_LINE_TYPE);

            var _obj = self.cacheGlobal.getUIItemByLineNumber(_rowLineNumber),       
            	_rowLineDesc = _obj[self.constants.FLD_LINE_DESCRIPTION];

            // Populate line type table
            self.resetHTML([
                self.lineTypeTable
            ]);

            var _row_linetype = $(
            	'<tr>' + '<td>' + _rowLineType + '</td>' + 
                '<td>' + _rowLineDesc +'</td>' + '</tr>'
            );
            $(self.lineTypeTable).append(_row_linetype);

            // Some rows have no ParsedFields info
            var _fieldObj = _obj[self.constants.FLD_PARSED_FIELDS];

	        if(_.isObject(_fieldObj)){
	            // Copy over the cell content to the text field
	            var _rawString = _cell.index() === 1 ? _cell.text() : _cell.next().text();
	            self.setRowDetails(_fieldObj, _rawString);  

	            // All td's in same row will be lit
	            self.highlightRow(_cell);

                // Initialize tooltips
                $('[data-toggle="tooltip"]').tooltip({
                                animation: false,
                                container: false
                            });

				return true;

	        } else {
                // Some line do not have ParsedFields node
                // Clear details section
                self.resetHTML([
                                self.detailsTable,
                                self.detailsRawString,
                                self.lineTypeTable
                            ]);

                return false;
            }
    	},

    	/**
         * Creates new rows and builds spans for the highlighting
         * 
         * @param {array}  dataArray
         * @param {string} rawLineString
         * 
         * @return void
         */
        setRowDetails : function(dataObject, rawLineString)
        {
        	var self = this;

            var _table = $(self.detailsTable),
                _divLine = $(self.detailsRawString);

            // Clear table and div
            self.resetHTML([
                _table,
                _divLine
            ]);

            _.each(dataObject, function(obj, key){

            	var _span = self.createSpanRange(obj, rawLineString);
		        _span.appendTo(_divLine);

		        var _row = self.createRow(obj);
		        _row.appendTo(_table);
            });
        },

        /**
         * Creates spans for each field section
         * 
         * @param  {object} fieldSec
         * @param  {string} rawStr
         * 
         * @return object
         */
        createSpanRange: function(fieldSec, rawStr)
        {

	    	var self 	 = this,
	    		_span    = $('<span>'),
                _curr    = fieldSec,
                _span_id = 'span-' + _curr[self.constants.FLD_FIELD_NAME];

            // JPM format index is 1-based
            _span.text(
                rawStr.substr(
                    _curr[self.constants.FLD_FIELD_START_INDEX] - 1,
                    _curr[self.constants.FLD_FIELD_LENGTH]
                )
            );

            _span.attr('id', _span_id);

            return _span;
        },

        /**
         * Creates the rows that map to each span
         * 
         * @param  {object} fieldKeyObj
         * 
         * @return object
         */
        createRow: function(fieldKeyObj)
        {
        	var self = this;

        	 var _curr  = fieldKeyObj,
                 _row   = $('<tr>'),
                 _tdPos = $('<td>'),
                 _tdFld = $('<td>'),
                 _tdVal = $('<td>');
                
            // create a custom attribute
            _row.attr('mapped-to', 'span-' + _curr[self.constants.FLD_FIELD_NAME]);

            // tooltip for field desc
            // add it to field td
            var _span = $('<span>');

            _span.text(_curr[self.constants.FLD_FIELD_NAME]);
            _span.attr({
                            'title' : _curr[self.constants.FLD_DESCRIPTION],
                            'data-toggle' : 'tooltip',
                            'href' : '#'
                        });

            _tdFld.append(_span);

            _tdPos.text(_curr[self.constants.FLD_FIELD_START_INDEX]);              
            _tdVal.text(_curr[self.constants.FLD_FIELD_VALUE]);

            _row.append(_tdPos, _tdFld, _tdVal);

            _row.on('click', function(e){

            	var _cell = $(e.target),
				    _row = _cell.parent('tr'),
				    _row_map = _row.attr('mapped-to');

				var _match_span = $(self.detailsRawString + ' span#' + _row_map);

				// Highlights the span range
				self.highlightSpanRange(_match_span);

				// Highlights the clicked row
				self.highlightRow(_cell);

            });

            return _row;
        },

        /**
         * Toggles highlight for specified span section
         * 
         * @param {object} cell
         * 
         * @return void
         */
        highlightSpanRange : function(cell)
        {
            var self = this;

            // Remove current selections
            $(cell).siblings().removeClass(self.detailsSpanClass);
            $(cell).addClass(self.detailsSpanClass);
        },

        /**
         * Toggles highlight for specified row
         * 
         * @param {object} cell
         * 
         * @return void
         */
        highlightRow : function(cell)
        {
            var self = this;
                
            // Remove current selections
            $(cell).parent("tr")
            	   .parent("tbody")
                   .find("td")
                   .removeClass(self.rowSelectClass);

            // Highlight needs to be element and siblings
            $(cell).addClass(self.rowSelectClass);
            $(cell).siblings().addClass(self.rowSelectClass);
        },

        /**
         * Get field list for every type 
         * 
         * @param {string} lineType
         * 
         * @return void
         */
        setFilterFields: function(lineType)
        {
        	var self = this,
        		_fields = self.cacheGlobal.getUIFieldnamesByLineType(lineType);

        	if(_fields.length > 0){

        		var _field_select = $(self.searchForm).find("select#search_filter_fields");
        		_field_select.html('');
        		_field_select.prop('disabled',false);

                for(var i = 0, len = _fields.length; i < len; i++ ) {

                    var _option = $('<option>');

                    _option.val(_fields[i]);
                    _option.text(_fields[i]);

                    _field_select.append(_option);

                }
            }
        },

        /**
         * Wrapped hide
         * 
         * @param {array} selector
         * @param {boolean} isBlock
         * 
         * @return void
         */
        hide: function(selector, isBlock)
        {
            var cssObj = (isBlock) ? { display : 'none' } : { visibility : 'hidden' };

            // Array of selectors
            _.each(selector, function(sel) {
                $(sel).css(cssObj);       
            });
        },

        /**
         * Wrapped show
         * 
         * @param {array} selector
         * @param {boolean} isBlock
         * 
         * @return void
         */
        show: function(selector, isBlock)
        {
            var cssObj = isBlock ? { display : 'block' } : { visibility : 'visible' };

            _.each(selector, function(sel) {
                $(sel).css(cssObj);       
            });
        },

        /**
         * Builds a default select dropdown
         * 
         * @param {array} selector
         * 
         * @return void
         */
        resetSelect: function(selector)
        {
            _.each(selector, function(elem, key) {

                if(!_.isEmpty(elem.defaultVal)){
                    $(elem.selector).html(
                        '<option value="">' +  elem.defaultVal + '</option>'
                    );    
                }

                $(elem.selector).val('');
                $(elem.selector).prop('disabled',elem.disable);
            });
        },
        
        /**
         * Wrapped .html('') call
         * 
         * @param {array} selector
         * 
         * @return void
         */
        resetHTML: function(selector)
        {
            _.each(selector, function(sel) {
                $(sel).html('');       
            });
        },

        /**
         * Wrapped .removeClass() call
         * 
         * @param {array} selector
         * 
         * @return void
         */
        resetHighlight: function(selectors)
        {
            _.each(selectors, function(elem, key) {
                $(elem.selector).removeClass(elem.remove);
            });
        },

        /**
         * Sets default display for the result area
         * 
         * @param {string} message
         * @param {string} target
         * 
         * @return object
         */
        setResultArea: function(message, target)
        {
            var _div = $('<div>'),
                _i = $('<i>');

            _div.addClass(Constants.view.INDEX_RESULT_DIVCLASS);
            _div.text(message);
            _i.addClass(Constants.view.INDEX_RESULT_ICLASS);
            _i.prependTo(_div);

            _div.appendTo(target);
        },

        /**
         * Wrapped display message for search results
         * 
         * @param {string} message
         * 
         * @return void
         */
        showMatchMessage: function(str)
        {
            $(Constants.view.VIEWER_FILTER_BLOCKSEARCH).html(
                '<i class="' + 
                Constants.view.INDEX_RESULT_ICLASS + 
                '"></i> ' + str);
                    
            this.show([Constants.view.VIEWER_FILTER_BLOCKSEARCH], false);
        },

        /**
         * Scroll to an element's location
         * 
         * @param {string} Id
         * 
         * @return object
         */
        scrollToFocus: function(Id)
        {
            // Need to use native DOM reference to element
            var _focus = document.getElementById(Id);
            _focus.scrollIntoView();

            return _focus;
        }

    };

    // Give this "class" static behaviour (call as Class.myMethod)
   	var UISingleton = UISingleton || new UI();
    return UISingleton;
});